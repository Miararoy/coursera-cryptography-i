from operator import xor
from binascii import hexlify, a2b_hex

_hex_to_binary_str = lambda x: "".join(reversed( [i+j for i,j in zip( *[ ["{0:04b}".format(int(c,16)) for c in reversed("0"+x)][n::2] for n in [1,0] ] ) ] ))
_hex_to_binary_list = lambda x: list(reversed( [i+j for i,j in zip( *[ ["{0:04b}".format(int(c,16)) for c in reversed("0"+x)][n::2] for n in [1,0] ] ) ] ))


def hex_to_binary(hex_str, return_type):
    if isinstance(hex_str,str):
        if return_type == str:
            return _hex_to_binary_str(hex_str)
        elif return_type == list:
            return _hex_to_binary_list(hex_str)
        else:
            raise TypeError("return_type should be str/list")
    else:
        raise TypeError("hex_str must be of type str")

binary_to_hex = lambda x: '{:0{}X}'.format(int(x, 2), len(x) // 4).lower()


def strxor(a, b):     # xor two strings of different lengths
    if len(a) > len(b):
       return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a[:len(b)], b)])
    else:
       return "".join([chr(ord(x) ^ ord(y)) for (x, y) in zip(a, b[:len(a)])])

def hex_addition(hex_str, adder):
        return (binary_to_hex(bin(int(hex_str.encode('hex'), 16) + adder))).decode('hex')